const express = require("express");
const assert = require("assert");
const router = express.Router();
const jwt = require("../helpers/jwt");
const db = require("../db/mysql-connector");

const QueryBuilder = require("../db/QueryBuilder");

const logger = require("tracer").dailyfile({
    root: "./logs",
    maxLogFiles: 10,
    allLogsFileName: "appartmentApp",
    format: "{{timestamp}} <{{title}}> {{message}} (in {{file}}:{{line}})",
    dateformat: "HH:MM:ss.L"
});

const Reservation = require("../models/Reservation");
const Apartment = require("../models/Apartment");

router.all("*", (req, res, next) => {
    assert(
        typeof req.headers["x-access-token"] == "string",
        "token is not a string!"
    );

    const token = req.header("X-Access-Token") || "";

    jwt.decodeToken(token, (err, payload) => {
        if (err) {
            logger.log("Error handler: " + err.message);
            res.status(401).json({error: "Access denied, need to login first"});
        } else {
            logger.log(`${token} `);
            next();
        }
    });
});

// Function for executing SQL queries
function executeQuery(query, res, succes) {
    db.query(query, (err, rows, fields) => {
        if (err) {
            logger.log(err);
            res
                .status(500)
                .json({error: `Could not get data from the database, ${err}`});
        } else {
            logger.log();

            if (succes != null) {
                res.status(200).json(succes);
            } else {
                res.status(200).json(rows)
            }
        }
    });
}

//
// Appartments endpoints
//

// Read all appartments in the database
router.get("/apartments", (req, res, next) => {
    try {
        //SQL statement
        const query = new QueryBuilder("apartment").select();

        executeQuery(query, res, null);
    } catch (ex) {
        logger.error(ex);
        res.status(500).json({error: ex});
    }
});

// Create apartment in database
router.post("/apartments", (req, res, next) => {
    try {
        // Req body json objects check
        assert(
            typeof req.body.description == "string",
            "description is not a string"
        );
        assert(
            typeof req.body.streetAddress == "string",
            "streetAddress is not a string!"
        );
        assert(
            typeof req.body.postalCode == "string",
            "postalCode is not a string!"
        );
        assert(typeof req.body.city == "string", "city is not a string");

        //SQL statement
        const description = req.body.description || "";
        const street = req.body.streetAddress || "";
        const postalCode = req.body.postalCode || "";
        const city = req.body.city || "";

        const userId = req.body.userId;

        const apartment = new Apartment(
            description,
            street,
            postalCode,
            city,
            userId
        );

        const query = new QueryBuilder("apartment").insert([
            {
                field: "Description",
                value: apartment.description
            },
            {
                field: "StreetAddress",
                value: apartment.streetAddress
            },
            {
                field: "PostalCode",
                value: apartment.postalCode
            },
            {
                field: "City",
                value: apartment.city
            },
            {
                field: "UserId",
                value: apartment.userId
            }
        ]);

        executeQuery(query, res, {msg: 'apartment has been created!'});
    } catch (ex) {
        logger.error(ex);
        res.status(500).json({error: ex});
    }
});

// Read specific apartment by id
router.get("/apartments/:id", (req, res, next) => {
    try {
        // Apartment id from req params
        const id = req.params.id;

        const query = new QueryBuilder("apartment")
            .where("ApartmentId", id)
            .select();

        executeQuery(query, res, null);
    } catch (ex) {
        logger.error(ex);
        res.status(500).json({
            error: ex,
            description: `Could not get apartment with id: ${appartmentId}`
        });
    }
});

// Update specific apartment by id
router.put("/apartments/:id", (req, res, next) => {
    try {
        // Req body json objects check
        assert(
            typeof req.body.description === "string",
            "description is not a string"
        );
        assert(
            typeof req.body.streetAddress === "string",
            "streetAddress is not a string!"
        );
        assert(
            typeof req.body.postalCode === "string",
            "postalCode is not a string!"
        );
        assert(typeof req.body.city === "string", "city is not a string");

        const id = req.params.id;

        const description = req.body.description || "";
        const street = req.body.streetAddress || "";
        const postalCode = req.body.postalCode || "";
        const city = req.body.city || "";

        // SQL statement
        const query = new QueryBuilder("apartment")
            .where("ApartmentId", id)
            .update([
                    {
                        field: 'Description',
                        value: description
                    },
                    {
                        field: 'StreetAddress',
                        value: street
                    },
                    {
                        field: 'PostalCode',
                        value: postalCode
                    },
                    {
                        field: 'City',
                        value: city
                    }
                ]
            );

        executeQuery(query, res, {msg: 'apartment info successfully changed!'});
    } catch
        (ex) {
        logger.error(ex);
        res.status(500).json({error: ex});
    }
})
;

// Delete specific apartment by id
router.delete("/apartments/:id", (req, res, next) => {
    try {
        // Id of apartment from req params
        const id = req.params.id;

        // SQL statement
        const query = new QueryBuilder("apartment")
            .where("ApartmentId", id)
            .delete();

        executeQuery(query, res, {msg: 'apartment has been successfully deleted!'});
    } catch (ex) {
        logger.error(ex);
        res.status(500).json({error: ex});
    }
});

//
// Reservations endpoints
//

// Create reservation for specific apartment
router.post("/apartments/:id/reservations", (req, res, next) => {
    try {
        // req body json objects check
        assert(
            typeof req.body.startDate === "string",
            "startDate is not a string/date!"
        );
        assert(
            typeof req.body.endDate === "string",
            "endDate is not a string/date!"
        );
        assert(typeof req.body.status === "string", "status is not a string!");

        const startDate = req.body.startDate || "";
        const endDate = req.body.endDate || "";
        const status = req.body.status || "";
        const userId = req.body.userId || "";

        const id = req.params.id;

        const reservation = new Reservation(startDate, endDate, status, userId, id);

        //SQL statemens
        const query = new QueryBuilder("reservation").insert([
            {
                field: "StartDate",
                value: reservation.startDate
            },
            {
                field: "EndDate",
                value: reservation.endDate
            },
            {
                field: "Status",
                value: reservation.status
            },
            {
                field: "UserId",
                value: reservation.userId
            },
            {
                field: "ApartmentId",
                value: reservation.apartmentId
            }
        ]);

        executeQuery(query, res, {msg: 'reservation successfully made!'});
    } catch (ex) {
        logger.error(ex);
        res.status(500).json({error: ex});
    }
});

// Read reservations of specific apartment
router.get("/apartments/:id/reservations", (req, res, next) => {
    try {
        // Id of apartment from req params
        const id = req.params.id || "";

        const query = new QueryBuilder("reservation")
            .where("ApartmentId", id)
            .select();

        executeQuery(query, res);
    } catch (ex) {
        logger.error(ex);
        res.status(500).json({error: ex});
    }
});

// Read specific reservation of specific apartment
router.get(
    "/apartments/:apartmentId/reservations/:reservationId",
    (req, res, next) => {
        try {
            // Id of apartment from req params
            const appartmentId = req.params.apartmentId || "";

            // Id of reservation from req params
            const reservationId = req.params.reservationId || "";

            // MySQL query
            const query = new QueryBuilder("reservation")
                .where("ApartmentId", appartmentId)
                .where("ReservationId", reservationId)
                .select();

            executeQuery(query, res);
        } catch (ex) {
            logger.error(ex);
            res.status(500).json({error: ex});
        }
    }
);

// Update the status of specific reservation of specific apartment
router.put(
    "/apartments/:apartmentId/reservations/:reservationId",
    (req, res, next) => {
        try {
            // Req body status object check
            assert(typeof req.body.status === "string", "status is not a string!");

            // Status string from req body
            let status = req.body.status;

            // Id of reservation from req params
            const reservationId = req.params.reservationId;
            //
            // // Id of apartment from req params
            const apartmentId = req.params.apartmentId;

            status = Reservation.validateStatus(status);

            // MySQL query
            const query = new QueryBuilder("reservation")
                .where("ReservationId", reservationId)
                .where("ApartmentId", apartmentId)
                .update([
                    {
                        field: 'Status',
                        value: status
                    }
                ]);

            executeQuery(query, res, {msg: 'reservation has been successfully changed!'});
        } catch (ex) {
            logger.error(ex);
            res.status(500).json({error: ex});
        }
    }
);

// Delete specific reservation
router.delete(
    "/apartments/:apartmentId/reservations/:reservationId",
    (req, res, next) => {
        try {
            const aId = req.params.apartmentId || "";
            const rId = req.params.reservationId || "";

            // SQL statement
            const query = new QueryBuilder("reservation")
                .where("ReservationId", rId)
                .where("ApartmentId", aId)
                .delete();


            executeQuery(query, res, {msg: 'reservation has been successfully deleted!'});
        } catch (ex) {
            logger.error(ex);
            res.status(500).json({error: ex});
        }
    }
);

// Fall back, display some info
router.all("*", (req, res) => {
    res.status(500).json({
        description: "Appartment App v1.0"
    });
});

module.exports = router;
