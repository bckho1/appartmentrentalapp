const express = require("express");
const assert = require("assert");
const router = express.Router();
const User = require("../models/User");
const db = require("../db/mysql-connector");
const bcrypt = require("bcryptjs");
const jwt = require("../helpers/jwt");

const QueryBuilder = require('../db/QueryBuilder');

const logger = require("tracer").dailyfile({
    root: "./logs",
    maxLogFiles: 10,
    allLogsFileName: "appartmentApp",
    format: "{{timestamp}} <{{title}}> {{message}} (in {{file}}:{{line}})",
    dateformat: "HH:MM:ss.L"
});

const saltRounds = 10;

//
// Register new user
//
router.post("/register", function (req, res, next) {
    try {
        const u = req.body;

        // Validate with assert is string etc ..
        assert(typeof u.firstName === "string", "firstName is not a string!");
        assert(typeof u.lastName === "string", "lastName is not a string!");
        assert(
            typeof u.streetAddress === "string",
            "streetAddress is not a string!"
        );
        assert(typeof u.postalCode === "string", "postalCode is not a string!");
        assert(typeof u.city === "string", "city is not a string!");
        assert(
            typeof u.dateOfBirth === "string",
            "dateOfBirth is not a string/date!"
        );
        assert(typeof u.phoneNumber === "string", "phoneNumber is not a string!");
        assert(typeof u.emailAddress === "string", "email is not a string!");
        assert(typeof u.password === "string", "password is not a string!");

        // Create new user object, hash password (do not store in db).
        // Throws err if no valid object can be constructed
        const hash = bcrypt.hashSync(req.body.password, saltRounds);

        const user = new User(
            u.firstName,
            u.lastName,
            u.streetAddress,
            u.postalCode,
            u.city,
            u.dateOfBirth,
            u.phoneNumber,
            u.emailAddress,
            hash
        );

        const query = new QueryBuilder('user').insert([
            {
                field: 'FirstName',
                value: user.firstName
            },
            {
                field: 'LastName',
                value: user.lastName
            },
            {
                field: 'StreetAddress',
                value: user.streetAddress
            },
            {
                field: 'City',
                value: user.city
            },
            {
                field: 'PostalCode',
                value: user.postalCode
            },
            {
                field: 'DataOfBirth',
                value: user.dateOfBirth
            },
            {
                field: 'PhoneNumber',
                value: user.phoneNumber
            },
            {
                field: 'EmailAddress',
                value: user.emailAddress
            },
            {
                field: 'Password',
                value: user.password
            },
        ]);

        // Perform query
        db.query(query, (err, rows, fields) => {
            if (err) {
                logger.log(err);
                res.status(500).json({error: "Register unsuccessful", description: `${err}`});
            } else {
                logger.log(`Account registered: ${user.firstName} ${user.lastName}`);
                res
                    .status(200)
                    .json({description: "Account successfuly registered!", rows});
            }
        });
    } catch (ex) {
        logger.log(ex);
        res.status(403).json({error: `${ex}`});
    }
});

//
// Login with username / password
//
router.post("/login", function (req, res, next) {
    try {
        // Validate with assert is string
        assert(
            typeof req.body.emailAddress === "string",
            "emailAddress is not a string!"
        );
        assert(typeof req.body.password === "string", "password is not a string!");

        // const u = req.body;

        // Construct query object
        // const query = {
        //   sql: "SELECT `Password` FROM `user` WHERE `EmailAddress`=?",
        //   values: [u.emailAddress],
        //   timeout: 2000
        // };

        const query = new QueryBuilder('user').where('EmailAddress', req.body.emailAddress).select();

        // Perform query
        db.query(query, (err, rows, fields) => {
            if (err) {
                res.status(500).json({error: `${err}`});
            } else {
                if (
                    rows.length === 1 &&
                    bcrypt.compareSync(req.body.password, rows[0].Password)
                ) {
                    logger.log(`${req.body.emailAddress} logged in.`);
                    token = jwt.encodeToken(req.body.emailAddress);
                    res
                        .status(200)
                        .json({description: "Successfully logged in!", token: token});
                } else {
                    logger.log(`Failed attempt login: ${req.body.emailAddress} | ${req.body.password}`);
                    res.status(400).json({error: `Failed login: ${rows}`});
                }
            }
        });
    } catch (ex) {
        res.status(500).json({error: `${ex}`});
    }
});

// Fall back, display some info
router.all("*", function (req, res, next) {
    res.status(404).json({Error: `Unknown endpoint!: ${res}`});
});

module.exports = router;
