var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../index.js');

chai.should();

chai.use(chaiHttp);

describe('Users', () => {

    it('Create new user and post', function (done) {
        chai.request(server)
            .post('/auth/register/')
            .send({
                "firstName": "Ben",
                "lastName": "Zak",
                "streetAddress": "Appelstraat 21",
                "postalCode": "4707AB",
                "city": "Eindhoven",
                "dateOfBirth": "1939-01-01",
                "phoneNumber": "0611223344",
                "emailAddress": "benzak@gmail.com",
                "password": "Adelaar123!"
            })
            .end((err, res, body) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                done()
            })
    });

    it('Login successfully', function (done) {
        chai.request(server)
            .post('/auth/login')
            .send({
                "emailAddress": "b.ivy@hotmail.com",
	            "password": "Appelmoes123!" 
            })
            .end(function (err, res) {
                res.should.have.status(200);
                res.body.should.be.a('object');
                done();
            })
    });
});
