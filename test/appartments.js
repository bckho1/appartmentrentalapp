var chai = require("chai");
var chaiHttp = require("chai-http");
var server = require("../index.js");

chai.should();

chai.use(chaiHttp);

describe("Appartments", () => {
  it("Create new appartment and post", done => {
    chai
      .request(server)
      .post("/apiv1/apartments/")
      .set("x-access-token", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1NTg4MTQ3MjYsImlhdCI6MTU1Nzk1MDcyNiwic3ViIjoiYi5pdnlAaG90bWFpbC5jb20ifQ.0E-XuGp3sySIJuMhPnvn7P1Q5Sk_3BQeuN4FqHj7DxE")
      .send({
        description: "Knus huisje",
        streetAddress: "Susannadonk 213",
        postalCode: "3434VB",
        city: "Best",
        userId: 2
      })
      .end((err, res, body) => {
        res.should.have.status(200);
        res.body.should.be.a('object');
        done();
      });
  });

  it("Login successfully", done => {
    chai
      .request(server)
      .post("/auth/login")
      .send({
        emailAddress: "b.ivy@hotmail.com",
        password: "Appelmoes123!"
      })
      .end(function(err, res) {
        res.should.have.status(200);
        res.body.should.be.a('object');
        done();
      });
  });

  it("Create new reservation and post", done => {
    chai
      .request(server)
      .post("/apiv1/apartments/12385/reservations")
      .set("x-access-token", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1NTg4MTQ3MjYsImlhdCI6MTU1Nzk1MDcyNiwic3ViIjoiYi5pdnlAaG90bWFpbC5jb20ifQ.0E-XuGp3sySIJuMhPnvn7P1Q5Sk_3BQeuN4FqHj7DxE")
      .send(
        {
          startDate: "2019-08-08",
          endDate: "2019-09-09",
          status: "DENIED",
          userId: 2
        }
      )
      .end((err, res, body) => {
        res.should.have.status(200);
        res.body.should.be.a('object');
        done();
      });
  });
});
