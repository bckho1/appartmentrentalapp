const config = require("./config/config.json");
const express = require("express");
const bodyParser = require("body-parser");
const apiv1 = require("./routes/apiv1");
const auth = require("./routes/auth");
const logger = require("tracer").dailyfile({
    root: "./logs",
    maxLogFiles: 10,
    allLogsFileName: "appartmentApp",
    format: "{{timestamp}} <{{title}}> {{message}} (in {{file}}:{{line}})",
    dateformat: "HH:MM:ss.L"
});

const app = express();

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

// Middelware: logging for all requests
app.all("*", function (req, res, next) {
    logger.info("%s", req.hostname, req.url);
    next();
});

// Routing without JWT
app.use("/auth", auth);

// Routing protected by JWT
app.use("/apiv1", apiv1);

app.all("*", (req, res, next) => {
    res.status(500).json({
        description: "Appartment App v1.0"
    });
})

// Optional log error
function errorLoggerHandler(err, req, res, next) {
    logger.error("%s", err.message);
    next(err);
}

// Set default error handler
function errorResponseHandler(err, req, res, next) {
    res.status(500).json({mgs: `Invalid action: ${req}`});
}

// Back-up error handlers
app.use(errorLoggerHandler);
app.use(errorResponseHandler);

// ECMA 6: server
const port = process.env.PORT || config.remote.port;
const server = app.listen(port, () => {
    console.log(
        `Appartment App is up and running at port ${server.address().port}`
    );
    logger.log(`Server starting on port ${server.address().port}`)
});

module.exports = app;
