class User {

    constructor(
        firstName,
        lastName,
        streetAddress,
        postalCode,
        city,
        dateOfBirth,
        phoneNumber,
        emailAddress,
        password
    ) {
        (this.firstName = this.validateFirstName(firstName)),
            (this.lastName = this.validateLastName(lastName)),
            (this.streetAddress = this.validateStreetAddress(streetAddress)),
            (this.postalCode = this.validatePostalCode(postalCode)),
            (this.city = this.validateCity(city)),
            (this.dateOfBirth = this.validateDateOfBirth(dateOfBirth)),
            (this.phoneNumber = this.validatePhoneNumber(phoneNumber)),
            (this.emailAddress = this.validateEmailAddress(emailAddress)),
            (this.password = this.validatePassword(password));
    }

    validateFirstName(firstName) {
        const regex = new RegExp(/^([A-Z])([a-z]{1,20})$/);
        if (regex.test(firstName)) {
            return firstName;
        } else {
            throw new Error(`Invalid firstname: ${firstName}`);
        }
    }

    validateLastName(lastName) {
        const regex = new RegExp(/^([A-Z])([a-z]{1,20})$/);
        if (regex.test(lastName)) {
            return lastName;
        } else {
            throw new Error(`Invalid lastName: ${lastName}`);
        }
    }

    //Dutch regex from https://murani.nl/blog/2015-09-28/nederlandse-reguliere-expressies/
    validateStreetAddress(streetAddress) {
        const regex = new RegExp(
            /^([1-9][e][\s])*([a-zA-Z]+(([\.][\s])|([\s]))?)+[1-9][0-9]*(([-][1-9][0-9]*)|([\s]?[a-zA-Z]+))?$/
        );
        if (regex.test(streetAddress)) {
            return streetAddress;
        } else {
            throw new Error(`Invalid streetAddress: ${streetAddress}`);
        }
    }

    validatePostalCode(postalCode) {
        const regex = new RegExp(/^[1-9][0-9]{3}[\s]?[A-Za-z]{2}$/);
        if (regex.test(postalCode)) {
            return postalCode;
        } else {
            throw new Error(`Invalid postalCode: ${postalCode}`);
        }
    }

    validateCity(city) {
        const regex = new RegExp(/^[a-zA-Z]+(?:[ '-][a-zA-Z]+)*$/);
        if (regex.test(city)) {
            return city;
        } else {
            throw new Error(`Invalid city: ${city}`);
        }
    }

    //TODO
    validateDateOfBirth(dateOfBirth) {
        const regex = new RegExp(
            /^([0-9]{2,4})-([0-1][0-9])-([0-3][0-9])(?:( [0-2][0-9]):([0-5][0-9]):([0-5][0-9]))?$/
        );
        if (regex.test(dateOfBirth)) {
            return dateOfBirth;
        } else {
            throw new Error(`Invalid dateOfBirth: ${dateOfBirth}`);
        }
    }

    validatePhoneNumber(phoneNumber) {
        const fixedPhoneNumberRegex = new RegExp(
            /^(((0)[1-9]{2}[0-9][-]?[1-9][0-9]{5})|((\\+31|0|0031)[1-9][0-9][-]?[1-9][0-9]{6}))$/
        );
        const mobilePhoneNumberRegex = new RegExp(
            /^(((\\+31|0|0031)6){1}[1-9]{1}[0-9]{7})$/
        );

        if (
            fixedPhoneNumberRegex.test(phoneNumber) ||
            mobilePhoneNumberRegex.test(phoneNumber)
        ) {
            return phoneNumber;
        } else {
            throw new Error(`Invalid phoneNumber: ${phoneNumber}`);
        }
    }

    //Official RFC 5322
    validateEmailAddress(emailAddress) {
        const regex = new RegExp(
            /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        );
        if (regex.test(emailAddress)) {
            return emailAddress;
        } else {
            throw new Error(`Invalid emailAddress: ${emailAddress}`);
        }
    }

    /*
      Password requires to be min. 6 characters long,
      contains 1 upper- and lowercase lettre,
      contains one number and contains one special character(#?!@$%^&*-)
      */
    validatePassword(password) {
        const regex = new RegExp(
            /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/
        );
        if (regex.test(password)) {
            return password;
        } else {
            throw new Error("Invalid password");
        }
    }
}

module.exports = User;
