class Reservation {
    constructor(startDate, endDate, status, userId, apartmentId) {
        (this.startDate = this.validateStartDate(startDate)),
            (this.endDate = this.validateEndDate(endDate)),
            (this.status = Reservation.validateStatus(status)),
            (this.userId = this.validateUserId(userId)),
            (this.apartmentId = this.validateApartmentId(apartmentId));
    }

    validateStartDate(startDate) {
        const regex = new RegExp(
            /^([0-9]{2,4})-([0-1][0-9])-([0-3][0-9])(?:( [0-2][0-9]):([0-5][0-9]):([0-5][0-9]))?$/
        );
        var currentDate = new Date();
        if (regex.test(startDate) && new Date(startDate) > currentDate) {
            return startDate;
        } else {
            throw new Error(`Invalid startDate: ${startDate}`);
        }
    }

    validateEndDate(endDate) {
        const regex = new RegExp(
            /^([0-9]{2,4})-([0-1][0-9])-([0-3][0-9])(?:( [0-2][0-9]):([0-5][0-9]):([0-5][0-9]))?$/
        );
        var currentDate = new Date();
        if (regex.test(endDate) && new Date(endDate) > currentDate) {
            return endDate;
        } else {
            throw new Error(`Invalid endDate: ${endDate}`);
        }
    }

    static validateStatus(status) {
        if (
            status == "DENIED" ||
            status == "ACCEPTED" ||
            status == "INITIAL"
        ) {
            return status;
        } else {
            throw new Error(`Invalid status: ${status}`);
        }
    }

    // First check with SQL statement in API route
    validateUserId(userId) {
        const regex = new RegExp(/^[0-9]*$/);
        if (regex.test(userId)) {
            return userId;
        } else {
            throw new Error(`UserId is not an integer: ${userId}`);
        }
    }

    // First check with SQL statement in API route
    validateApartmentId(apartmentId) {
        const regex = new RegExp(/^[0-9]*$/);
        if (regex.test(apartmentId)) {
            return apartmentId;
        } else {
            throw new Error(`Appartment is not an integer: ${appartmentId}`);
        }
    }
}

module.exports = Reservation;
