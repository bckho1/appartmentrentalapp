class Apartment {
    constructor(description, streetAddress, postalCode, city, userId) {
        (this.description = this.validateDescription(description)),
            (this.streetAddress = this.validateStreetAddress(streetAddress)),
            (this.postalCode = this.validatePostalCode(postalCode)),
            (this.city = this.validateCity(city)),
            (this.userId = this.validateUserId(userId));
    }

    validateDescription(description) {
        const regex = new RegExp(/^(.|\s)*[a-zA-Z]+(.|\s)*$/);
        if (regex.test(description)) {
            return description;
        } else {
            throw new Error(`Invalid description: ${description}`);
        }
    }

    //Dutch regex from https://murani.nl/blog/2015-09-28/nederlandse-reguliere-expressies/
    validateStreetAddress(streetAddress) {
        const regex = new RegExp(
            /^([1-9][e][\s])*([a-zA-Z]+(([\.][\s])|([\s]))?)+[1-9][0-9]*(([-][1-9][0-9]*)|([\s]?[a-zA-Z]+))?$/
        );
        if (regex.test(streetAddress)) {
            return streetAddress;
        } else {
            throw new Error(`Invalid streetAddress: ${streetAddress}`);
        }
    }

    validatePostalCode(postalCode) {
        const regex = new RegExp(/^[1-9][0-9]{3}[\s]?[A-Za-z]{2}$/);
        if (regex.test(postalCode)) {
            return postalCode;
        } else {
            throw new Error(`Invalid postalCode: ${postalCode}`);
        }
    }

    validateCity(city) {
        const regex = new RegExp(/^[a-zA-Z]+(?:[ '-][a-zA-Z]+)*$/);
        if (regex.test(city)) {
            return city;
        } else {
            throw new Error(`Invalid city: ${city}`);
        }
    }

    // First check with SQL statement in API route
    validateUserId(userId) {
        const regex = new RegExp(/^[0-9]*$/);
        if (regex.test(userId)) {
            return userId;
        } else {
            throw new Error(`UserId is not an integer: ${userId}`);
        }
    }
}

module.exports = Apartment;
