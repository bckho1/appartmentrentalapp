class QueryBuilder {

    constructor(table) {
        this._table = table;
        this._wheres = [];
    }

    where(field, value) {
        this._wheres.push({"field": field, "value": value});

        return this;
    }

    insert(values) {
        return {
            sql: 'INSERT INTO `' + this._table + '` (' + this.getFields(values) + ') VALUES (' + this.getValues(values) + ')',
            timeout: 2000
        };
    }

    delete() {
        return {
            sql: 'DELETE FROM `' + this._table + '` ' + this.getWheres(),
            timeout: 2000
        }
    }

    update(fields) {
        return {
            sql: 'UPDATE `' + this._table + '` SET ' + this.getSets(fields) + ' ' + this.getWheres(),
            timeout: 2000
        }
    }

    select() {
        return {
            sql: 'SELECT * FROM `' + this._table + '` ' + this.getWheres(),
            timeout: 2000
        }
    }

    getFields(values) {
        let v = "";

        values.forEach(element => {
            v += element.field + ", "
        });

        return v.substr(0, v.length - 2);
    }

    getValues(values) {
        let v = "";

        values.forEach(element => {
            v += "'" + element.value + "', "
        });

        return v.substr(0, v.length - 2);
    }

    getSets(fields) {
        let query = "";

        fields.forEach(element => {
            query += "`" + element.field + "` = '" + element.value + `', `
        });

        return query.substr(0, query.length - 2);
    }

    getWheres() {
        if (this._wheres.length > 0) {

            let query = "WHERE ";

            this._wheres.forEach((element => {
                query += "`" + element.field + "` = '" + element.value + "' AND ";
            }));

            return query.substr(0, query.length - 5);
        } else {
            return "";
        }
    }
}

module.exports = QueryBuilder;